﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinalTest_Track : MonoBehaviour
{
    [SerializeField] private GameObject m_Gate;
    [SerializeField] private Text m_TotalGates;

    private List<GameObject> platforms = new List<GameObject>();


    // Start is called before the first frame update
    private void Start()
    {
        platforms.Add(m_Gate);

        float range = 0;
        for (int i = 0; i < 12; i++)
        {

            GameObject obj = Instantiate(m_Gate, transform);
            Vector3 pos = Vector3.zero;
            pos.z = 4 * (i + 1); // + range;
            pos.x = Random.Range(-1, 2);
            obj.transform.position = pos;
            obj.name = $"Platform_{i + 1}";
            platforms.Add(obj);
        }

        m_TotalGates.text = platforms.Count.ToString();
    }

    public GameObject FirstPlatform()
    {
        GameObject firstPlatform = platforms[0];
        return firstPlatform;
    }

    public GameObject SelectPlatform(int id)
    {
        if (id >= platforms.Count)
        {
            return null;
        }
        GameObject firstPlatform = platforms[id];
        return firstPlatform;
    }


    public bool IsGateHit(Vector3 position, GameObject nextGate)
    {
        if (position.x < nextGate.transform.GetChild(0).position.x && position.x > nextGate.transform.GetChild(2).position.x)
        {
            //Debug.Log($"Left {nextGate.transform.GetChild(0).name} / Right {}");
            return true;
        }
        else
        {
            return false;
        }
    }
    
//TODO check this method
    public bool IsBallInPlatform(Vector3 position, ref GameObject nearestPlatform)
    {
        position.y = 0f;
        //nearestPlatform = platforms[0];
        int a = platforms.IndexOf(nearestPlatform);
        if (nearestPlatform.GetComponent<HopPlatform>().Parameters.JumpDistance > 0)
        {
            a += 1;
        }

        if (a + 1 >= platforms.Count)
        {
            return false;
        }

        for (int i = a + 1; i < platforms.Count; i++)
        {
            var platformZ = platforms[i].transform.position.z;
            if (platformZ + 0.5f < position.z)
            {
                continue;
            }

            if (platformZ - position.z > 0.5f)
            {
                continue;
            }

            nearestPlatform = platforms[i];
            break;
        }


        float minX = nearestPlatform.transform.position.x - 0.5f;
        float maxX = nearestPlatform.transform.position.x + 0.5f;

        if (position.x >= minX && position.x <= maxX)
        {
            var platform = nearestPlatform.GetComponent<HopPlatform>();
            platform.SetGreen();
            nearestPlatform.name = $"{nearestPlatform.name} passed";

            return true;
        }
        else
        {
            var platform = nearestPlatform.GetComponent<HopPlatform>();
            platform.SetRed();
            nearestPlatform.name = $"{nearestPlatform.name} failed";


            return false;
        }
    }
}
