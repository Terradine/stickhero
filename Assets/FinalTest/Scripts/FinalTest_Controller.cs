﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FinalTest_Controller : MonoBehaviour
{
    [SerializeField] private FinalTest_Player m_Player;
    [SerializeField] private Button m_StartButton;

    private void Start()
    {
        m_StartButton.onClick.AddListener(StartGame);
        m_Player.gameObject.SetActive(false);
    }

    public void StartGame()
    {
        m_Player.gameObject.SetActive(true);
        m_StartButton.gameObject.SetActive(false);
    }

    public void EndGame()
    {
        
    }
    
    
}
