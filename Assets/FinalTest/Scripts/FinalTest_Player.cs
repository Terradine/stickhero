﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FinalTest_Player : MonoBehaviour
{
    [SerializeField] private AnimationCurve m_JumpCurve;
    [SerializeField] private FinalTest_Input m_Input;
    [SerializeField] private FinalTest_Track m_Track;

    [SerializeField] private Text m_CurrentGate;
    [SerializeField] private Text m_Lifes;

    public int Score;
    
    private GameObject nextGate;

    private int lifes = 3;
    private int gateCount = 0;
    private float _iteration;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (nextGate == null)
        {
            nextGate = m_Track.FirstPlatform();
        }
        
        var gate = nextGate.GetComponent<FinalTest_Obstacle>();
        
        
        var pos = transform.position;
        pos.x = Mathf.Lerp(pos.x, m_Input.Strafe, Time.deltaTime * 5f);
        pos.z += Time.deltaTime * 5f;
        pos.y = m_JumpCurve.Evaluate(_iteration)*0.5f;
        if (_iteration < 1f)
        {
            _iteration += Time.deltaTime * 2;
        }
        else
        {
            _iteration = 0;
        }

        
        
        
        transform.position = pos;

        if (nextGate.transform.position.z - transform.position.z < 0.1f)
        {
            if (m_Track.IsGateHit(transform.position, nextGate))
            {
                gateCount++;
                nextGate.gameObject.SetActive(false);
                nextGate = m_Track.SelectPlatform(gateCount);
                
                if (nextGate == null)
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                }
                Debug.Log(gateCount);
            }
            else
            {
                gateCount++;
                nextGate = m_Track.SelectPlatform(gateCount);
                lifes--;
            }
        }
        

        m_CurrentGate.text = gateCount.ToString();
        m_Lifes.text = lifes.ToString();
        if (lifes == 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
