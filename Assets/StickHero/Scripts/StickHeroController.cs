﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class StickHeroController : MonoBehaviour
{
    [SerializeField] private StickHeroStick m_Stick;
    [SerializeField] private StickHeroPlayer m_Player;
    [SerializeField] private StickHeroPlatform m_Platform;
    private List<StickHeroPlatform> m_Platforms;
    
    private int counter; //счетчик платформ
    private float _marginX; 
    
    private enum EGameState
    {
        Wait,
        Scaling,
        Rotate,
        Movement,
        Defeat,
    }

    private EGameState currentGameState;
    
    // Start is called before the first frame update
    private void Start()
    {
        currentGameState = EGameState.Wait;
        counter = 0;

        m_Platforms = new List<StickHeroPlatform>();

        _marginX = 0;
        for (int i = 0; i<7 ; i++)
        {
            CreateNewPlatform();
        }
        
        m_Stick.ResetStick(m_Platforms[0].GetStickPosition());
        m_Player.SetInititialPlayerPos(m_Platforms[0].GetPlayerInitPos());
    }

    // Update is called once per frame
    private void CreateNewPlatform()
    {
        StickHeroPlatform newPlatform = gameObject.AddComponent<StickHeroPlatform>() as StickHeroPlatform;
            
                
        Vector3 offset = new Vector3(_marginX, 0, 0);
        newPlatform = Instantiate(m_Platform, transform.position + offset, Quaternion.identity);
            
        float scaleX = Random.Range(0.5f, 1.5f);
            
        newPlatform.transform.localScale = new Vector3(scaleX,2.2375f,1f);
            
        float coordX = Random.Range(1f, 2f);
        _marginX = _marginX + coordX + scaleX; 
        m_Platforms.Add(newPlatform);
    }
    
    private void Update()
    {
        if (!Input.GetMouseButtonDown(0))
        {
            return;
        }

        switch (currentGameState)
        {
            case EGameState.Wait:
                currentGameState = EGameState.Scaling;
                StickHeroPlatform currentPlatform = m_Platforms[counter];
                StickHeroPlatform nextPlatform = m_Platforms[counter + 1];
                float maxLength = nextPlatform.GetStickPosition().x - currentPlatform.GetStickPosition().x + 0.5f;
                
                m_Stick.StartScaling(maxLength);
                break;
            
            case EGameState.Scaling:
                currentGameState = EGameState.Rotate;
                m_Stick.StopScaling();
                break;
            
            case EGameState.Rotate:
            case EGameState.Movement:
                break;
            
            case EGameState.Defeat:
                print("Game restarted");
                int activeSceneIndex = SceneManager.GetActiveScene().buildIndex;
                SceneManager.LoadScene(activeSceneIndex);
                break;
            
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public void StopStickScale()
    {
        currentGameState = EGameState.Rotate;
        m_Stick.StartRotate();
    }

    //TODO выкинуть?
    public void StopStickRotate()
    {
        currentGameState = EGameState.Movement;
    }

    public void StartPlayerMovement(float length)
    {
        currentGameState = EGameState.Movement;
        StickHeroPlatform nextPlatform = m_Platforms[counter + 1];
        
        
        //находим минимальную длину стика для успешного перехода
        float targetLength = nextPlatform.transform.position.x -
                             m_Stick.transform.position.x;
        float platformSize = nextPlatform.GetPlatformSize();
        
        float min = targetLength - platformSize * 0.5f;
        min -= m_Player.transform.localScale.x;
        
        //находим максимальную длину стика
        float max = targetLength + platformSize * 0.5f;
        
        //при успехе игрок переходит в центр платформы, иначе падаем
        if (length > min && length < max)
        {
            m_Player.StartMovement(
                nextPlatform.transform.position.x, false);
        }
        else
        {
            float targetPos = m_Stick.transform.position.x +
                              length + m_Player.transform.localScale.x;
            
            m_Player.StartMovement(targetPos, true);
        }
        
        
        //adding new object
        CreateNewPlatform();



    }

    public void StopPlayerMovement()
    {
        currentGameState = EGameState.Wait;
        counter++;
        m_Stick.ResetStick(m_Platforms[counter].GetStickPosition());
    }

    public void ShowScores()
    {
        currentGameState = EGameState.Defeat;
        print($"Game over {counter}");
    }
}
