﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickHeroPlatform : MonoBehaviour
{
    [SerializeField] private Transform m_stickPoint;

    
    public Vector3 GetStickPosition()
    {
        return m_stickPoint.position;
    }

    public float GetPlatformSize()
    {
        return transform.localScale.x;
    }

    public Vector3 GetPlayerInitPos()
    {
       // float coordX = transform.position.x + transform.localScale.x *0.5f;
        Vector3 pos = new Vector3(transform.position.x, m_stickPoint.position.y+0.24f,0);
        return pos;
    }
}
