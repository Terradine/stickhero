﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FingerDriverTrack : MonoBehaviour
{
    private class TrackSegment
    {
        public Vector3[] Points;

        public bool IsPointInSegment(Vector3 point)
        {
            return MathfTriangles.IsPointInTriangleXY(point, Points[0], Points[1], Points[2]);
        }
    }

    [SerializeField] private LineRenderer m_lineRenderer;
    [SerializeField] private bool m_debug;

    private Vector3[] corners;
    private TrackSegment[] segments;
    
    
    // Start is called before the first frame update
    private void Start()
    {
        //waypoints array
        corners = new Vector3[transform.childCount];
        for (int i = 0; i < corners.Length; i++)
        {
            GameObject obj = transform.GetChild(i).gameObject;
            corners[i] = obj.transform.position;
            //hide object
            obj.GetComponent<MeshRenderer>().enabled = false;
        }
        
        //configuring Line Renderer
        m_lineRenderer.positionCount = corners.Length;
        m_lineRenderer.SetPositions(corners);
        
        //Backing mesh
        Mesh mesh = new Mesh();
        m_lineRenderer.BakeMesh(mesh,true);
        
        //creating array of track segments (triangle, 3 points)
        segments = new TrackSegment[mesh.triangles.Length/3];
        int segmentCounter = 0;

        for (int i = 0; i < mesh.triangles.Length; i += 3)
        {
            segments[segmentCounter] = new TrackSegment();
            segments[segmentCounter].Points = new Vector3[3];
            segments[segmentCounter].Points[0] = mesh.vertices[mesh.triangles[i]];
            segments[segmentCounter].Points[1] = mesh.vertices[mesh.triangles[i+1]];
            segments[segmentCounter].Points[2] = mesh.vertices[mesh.triangles[i+2]];
            segmentCounter++;
        }
        
        //debug
        if (!m_debug)
        {
            return;
        }

        foreach (var segment in segments)
        {
            foreach (var point in segment.Points)
            {
                GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                sphere.transform.position = point;
                sphere.transform.localScale = Vector3.one * 0.1f;

            }
        }
    }

    // Update is called once per frame
    /// <summary>
    /// check if point is on track
    /// </summary>
    /// <param name="point">point</param>
    /// <returns></returns>
    public bool isPointInTrack(Vector3 point)
    {
        foreach (var segment in segments)
        {
            if (segment.IsPointInSegment(point))
            {
                return true;
            }
        }
        return false;
    }
}
