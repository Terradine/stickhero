﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FingerDriverInput : MonoBehaviour
{
    [SerializeField] 
    private Transform m_steerWheelTranform;
    
    [SerializeField] 
    [Range(0, 180f)] 
    private float m_maxSteerAngle = 90f; 

    [SerializeField] 
    [Range(0, 1f)] 
    private float m_maxSteerAcceleration = 0.25f;

    private float steerAxis;

    public float SteerAxis
    {
        get => steerAxis;
        set => steerAxis = Mathf.Lerp(steerAxis, value, m_maxSteerAcceleration);
    }

    private Vector2 startSteerWheelPoint;
    private Camera mainCamera;
    
    
    // Start is called before the first frame update
    private void Start()
    {
        mainCamera=Camera.main;
        startSteerWheelPoint = mainCamera.WorldToScreenPoint(m_steerWheelTranform.position);
        
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            //angle between steer wheel and touch point on screen
            float angle = Vector2.Angle(Vector2.up, (Vector2)Input.mousePosition - startSteerWheelPoint);

            angle /= m_maxSteerAngle;
            angle = Mathf.Clamp01(angle);

            if (Input.mousePosition.x > startSteerWheelPoint.x)
            {
                angle *= -1f;
            }

            SteerAxis = angle;
        }
        else
        {
            SteerAxis = 0;
        }
        m_steerWheelTranform.localEulerAngles = new Vector3(0f,0f,steerAxis * m_maxSteerAngle);
        
    }
}
