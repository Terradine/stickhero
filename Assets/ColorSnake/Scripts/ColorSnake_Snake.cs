﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorSnake_Snake : MonoBehaviour
{
    [SerializeField] private ColorSnake_GameController m_GameController;
    [SerializeField] private SpriteRenderer m_SpriteRenderer;

    private Vector3 _position;
    private int currentType;
    private Vector3 _controlStartPoint = Vector3.zero;
    private Vector3 _controlCurrentPosition = Vector3.zero;
    private float _axisLength;


    private void Start()
    {
        _position = transform.position;

        var colorType = m_GameController.Types.GetRandomColorType();
        currentType = colorType.Id;
        m_SpriteRenderer.color = colorType.Color;
    }

    private void SetColor(int id)
    {
        var colorType = m_GameController.Types.GetColorType(id);
        currentType = colorType.Id;
        m_SpriteRenderer.color = colorType.Color;
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var obstacle = other.gameObject.GetComponent<ColorSnake_Obstacle>();
        if (!obstacle)
        {
            return;
        }

        if (obstacle.IsColorChanger)
        {
            SetColor(obstacle.ColorId);
            return;
        }

        if (obstacle.ColorId == currentType)
        {
            Destroy(obstacle.gameObject);    
            m_GameController.SetScore(1); //basic scoring: 1 obstacle = 1 point 
        }
        else
        {
            m_GameController.GameOver();
        }
        
        //SetColor(obstacle.ColorId);
        
    }

    // Update is called once per frame
    void Update()
    {
        _position = transform.position;
        if (!Input.GetMouseButton(0))
        {
            return;
        }

        
        
        float direction;
        float deviation;
        
        if (Input.GetMouseButtonDown(0)) 
        { 
            _controlStartPoint.x = m_GameController.MainCamera.ScreenToWorldPoint(Input.mousePosition).x;
            _axisLength = Mathf.Abs(m_GameController.Bounds.Left) - Mathf.Abs(_controlStartPoint.x);
        }

        _controlCurrentPosition = m_GameController.MainCamera.ScreenToWorldPoint(Input.mousePosition);
        if (_controlCurrentPosition.x < _controlStartPoint.x)
        {
            direction = -1f;
            deviation = _controlStartPoint.x - _controlCurrentPosition.x;
        }
        else
        {
            direction = 1f;
            deviation = _controlCurrentPosition.x - _controlStartPoint.x;
        }

        float min = m_GameController.Bounds.Left;
        float max = m_GameController.Bounds.Right;

        _position.x = Mathf.Clamp(_position.x + direction * deviation *0.3f * _axisLength/max, min, max);
        transform.position = _position;

       // Debug.Log(_axisLength);
        

        /*        
        float min = m_GameController.Bounds.Left;
        float max = m_GameController.Bounds.Right;       
       
        
        _position.x = Mathf.Clamp(_position.x, min, max);        
        transform.position = _position;
        */
    }
}
