﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorSnake_Obstacle : MonoBehaviour
{
    [HideInInspector]
    public int ColorId;
    [HideInInspector]
    public bool IsColorChanger;
    [HideInInspector]
    public bool IsRotating;

    private void Update()
    {
        if (IsRotating == true)
        {
            var angles = transform.rotation.eulerAngles;
            angles.z += Time.deltaTime * 50;
            transform.rotation = Quaternion.Euler(angles);
        }
    }
}
