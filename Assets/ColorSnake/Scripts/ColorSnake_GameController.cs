using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ColorSnake_GameController : MonoBehaviour
{
    //Bounds of screen from camera position
    public class CameraBounds
    {
        public float Left;
        public float Right;
        public float Up;
        public float Down;
    }
    
    [SerializeField] private Camera m_MainCamera;
    public Camera MainCamera => m_MainCamera;
    [SerializeField] private ColorSnake_Snake m_Snake;
    [SerializeField] private ColorSnake_Types m_Types;
    [SerializeField] private float m_Speed = 2f;
    public ColorSnake_Types Types => m_Types;
    private CameraBounds _bounds;
    private int _score = 0;

    public CameraBounds Bounds
    {
        get => _bounds;
        private set => _bounds = value;
    }

    private void Awake()
    {
        Vector2 minScreen = m_MainCamera.ScreenToWorldPoint(Vector3.zero);
        _bounds = new CameraBounds()
        {
            Left = minScreen.x,
            Right = Mathf.Abs(minScreen.x),
            Down = minScreen.y,
            Up = Mathf.Abs(minScreen.y)
        };
    }

    private void Update()
    {
        Vector3 movement = Time.deltaTime * m_Speed * Vector3.up;
        m_MainCamera.transform.Translate(movement);
        m_Snake.transform.Translate(movement);
    }

    private void Reset()
    {
        m_MainCamera = Camera.main;
    }

    public void SetScore(int points)
    {
        _score += points;
    }

    public void GameOver()
    {
        Debug.Log($"Game over! Score : {_score}");
        _score = 0;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
