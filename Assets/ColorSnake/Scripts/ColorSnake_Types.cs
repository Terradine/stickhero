﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;


//object colors
[Serializable]
public class ColorType
{
   public int Id;
   public string Name;
   public Color Color;
}

//types
[Serializable]
public class ObjectType
{
   public int Id;
   public string Name;
   public GameObject Object;
   public bool GoodColorChanger;
}

//templates
[Serializable]
public class TemplateType
{
   public int Id;
   public string Name;
   public Transform[] Points;
   public bool IsColorChanger;

}

public class ColorSnake_Types : MonoBehaviour
{
   [SerializeField] private ColorType[] m_Colors;
   [SerializeField] private ObjectType[] m_Objects;
   [SerializeField] private TemplateType[] m_Templates;

   public ColorType GetRandomColorType()
   {
      int rand = Random.Range(0, m_Colors.Length);
      return m_Colors[rand];
   }
   
   public ObjectType GetRandomObjectType(bool colorChanger)
   {
      List<ObjectType> objects = new List<ObjectType>();
      if (colorChanger)
      {
         objects = m_Objects.Where(t=>t.GoodColorChanger == true).ToList();   
      }
      else
      {
         objects = m_Objects.ToList();
      }
         
      
      int rand = Random.Range(0, objects.Count);
      return objects[rand];
   }
   
   public TemplateType GetRandomTemplate(bool colorChanger)
   {
      List<TemplateType> templates = m_Templates.Where(t=>t.IsColorChanger == colorChanger).ToList();
      
      int rand = Random.Range(0, templates.Count);
      return templates[rand];
   }

   public ColorType GetColorType(int id)
   {
      return m_Colors.FirstOrDefault(c => c.Id == id);
   }

   public ObjectType GetObjectType(int id)
   {
      return m_Objects.FirstOrDefault(d => d.Id == id);
   }
}
