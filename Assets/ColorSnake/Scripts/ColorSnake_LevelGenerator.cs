﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ColorSnake_LevelGenerator : MonoBehaviour
{
    [SerializeField] private ColorSnake_Types m_Types;
    [SerializeField] private ColorSnake_GameController m_Controller;
    [SerializeField] private float m_Margin = 3f;

    private int line = 1;//номер генерируей линии препятствий
    private List<GameObject> obstacles = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        var upBorder = m_Controller.Bounds.Up;
        while (line * m_Margin < upBorder + m_Margin)
        {
            GenerateObstacles();
        }
    }

    void Update()
    {
        var upBorder = m_Controller.Bounds.Up + m_Controller.MainCamera.transform.position.y;
        if (line * m_Margin > upBorder + m_Margin)
            return;
        if (line % 5 == 0)
        {
            GenerateColorChanger();
        }
        else
        {
            GenerateObstacles();    
        }
        
        var downBorder = m_Controller.MainCamera.transform.position.y + m_Controller.Bounds.Down;
        if(obstacles[0].transform.position.y + 4f < downBorder)
        {
            Destroy(obstacles[0]);
            obstacles.RemoveAt(0);
        }
        
    }
    private void GenerateObstacles()
    {
        var template = m_Types.GetRandomTemplate(false);
        var obstacle = new GameObject("Obstacle_" + line);
        foreach (var point in template.Points)
        {
            var objType = m_Types.GetRandomObjectType(false);
            var colorType = m_Types.GetRandomColorType();

            var obj = Instantiate(objType.Object, point.transform.position, point.transform.rotation);
            obj.transform.parent = obstacle.transform;
            
            
           
            
            obj.GetComponent<SpriteRenderer>().color = colorType.Color;

            var obstacleController = obj.AddComponent<ColorSnake_Obstacle>();
            obstacleController.ColorId = colorType.Id;
            int rand = Random.Range(0, 6);
            switch (rand)
            {
                case 0:
                case 1:
                    obstacleController.IsRotating = true;
                    break;
                default:
                    obstacleController.IsRotating = false;
                    break;
            }
            
        }

        Vector3 pos = obstacle.transform.position;
        pos.y = line * m_Margin;//2 - расстояние между препятсвиями
        obstacle.transform.position = pos;

        line++;

        obstacles.Add(obstacle);
    }

    private void GenerateColorChanger()
    {
        var template = m_Types.GetRandomTemplate(true);
        var obstacle = new GameObject("Obstacle_NewColor_" + line);
        var point = template.Points.FirstOrDefault();
       
        var objType = m_Types.GetRandomObjectType(true);
        var colorType = m_Types.GetRandomColorType();
        var obj = Instantiate(objType.Object, point.transform.position, point.transform.rotation);
        
        obj.transform.localEulerAngles = new Vector3(0f, 0f, -90f);
        obj.transform.localScale += new Vector3(0, 100,0);
        
        obj.transform.parent = obstacle.transform;
            

        obj.GetComponent<SpriteRenderer>().color = colorType.Color;

        var obstacleController = obj.AddComponent<ColorSnake_Obstacle>();
        obstacleController.ColorId = colorType.Id;
        obstacleController.IsColorChanger = true;
       
        
        Vector3 pos = obstacle.transform.position;
        pos.y = line * m_Margin;//2 - расстояние между препятсвиями
        obstacle.transform.position = pos;

        line++;

        obstacles.Add(obstacle);
    }
}

