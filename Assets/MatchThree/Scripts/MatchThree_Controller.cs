﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR.WSA.Input;

public class MatchThree_Controller : MonoBehaviour
{
    [SerializeField] private MatchThree_Field m_Field;
    [SerializeField] private MatchThree_Types m_Types;
    //[SerializeField] private Button m_ResetButton;
    [SerializeField] private Text m_Score;

    public static Camera MainCamera;
    public static int Score=0;
    public static int TopScore = 0;

    
    
    /// <summary>
    ///проверяем можем ли мы поместить в данную ячейку такую кофету -  нужно избегать ситуаций с 3 и более в ряд на старте игры
    /// </summary>
    /// <param name="targetCell">Стартовая клетка</param>
    /// <param name="id">Id конфеты</param>
    /// <returns></returns>
    public static bool IsFreeCandyPlacement(MatchThree_Cell targetCell, int id)
    {
        //Direction от 0 до 4 соответствует enum
        
        for (int i = 0; i < 4; i++)
        {
            Direction direction = (Direction) i;
            int counter = 0;
            int repeated = 0;

            MatchThree_Cell cell = targetCell;
            Direction oppositeDirection = GetOppositeDirection(direction);

            int cellsToCheck = 2;
            try
            {
                MatchThree_Cell cellOpposite = cell.GetNeighbour(oppositeDirection);
                if (cellOpposite.Candy.CandyData.Id == id)
                {
                    repeated++;
                    cellsToCheck -= 1;
                }
            }
            catch
            {
                //just skip it
            }
            while (counter < cellsToCheck)
            {
                cell = cell.GetNeighbour(direction);
                if (!cell || !cell.Candy)
                {
                    break;
                }
                if (cell.Candy.CandyData.Id == id)
                {
                    repeated++;
                }
                counter++;
            }

            if (repeated >= 2)
            {
                return false;
            }
        }
        return true;
    }

    public static void DestroyLine(MatchThree_Cell targetCell)
    {
        Score++;
        Direction direction = GetLineDirection(targetCell);
        List<MatchThree_Cell> candyLine = GetLine(direction, targetCell);

        //MatchThree_Types types = new MatchThree_Types();
        
        
        if (candyLine.OrderByDescending(d => d.YPos).FirstOrDefault().YPos == MatchThree_Field.YPosMax)
        {
            //TODO Generate new candies
            foreach (var cell in candyLine)
            {
                Destroy(cell.Candy.gameObject);
                //candy.Candy = types.GetRandomCandy();
            
            }
        }
        else
        {
            //TODO Generate new candies
            foreach (var cell in candyLine.OrderBy(d=>d.YPos))
            {
                Destroy(cell.Candy.gameObject);
                MatchThree_Cell nextCell = cell;
                do
                {
                    nextCell.Candy = nextCell.GetNeighbour(Direction.Up).Candy;
                    try
                    {
                        nextCell.Candy.transform.position = nextCell.transform.position;
                    }
                    catch (Exception e)
                    {
                        
                    }
                    cell.GetNeighbour(Direction.Up).Candy = null;
                    nextCell = nextCell.GetNeighbour(Direction.Up);

                } while (nextCell.YPos < MatchThree_Field.YPosMax);
                    
                

            }
            
            
            
            //move down, generate new
        }

    }

    public void Restart()
    {
        if (TopScore < Score)
        {
            TopScore = Score;
        }
        Score = 0;
        
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        
    }
    private static Direction GetOppositeDirection(Direction direction)
    {
        int oppositeDirection=-1;
        switch ((int)direction)
        {
            case 0: oppositeDirection = 2;
                break;
            case 1: oppositeDirection = 3;
                break;
            case 2: oppositeDirection = 0;
                break;
            case 3: oppositeDirection = 1;
                break;
            default: break;
        }

        return (Direction) oppositeDirection;
    }

    private static List<MatchThree_Cell> GetLine(Direction direction, MatchThree_Cell targetCell)
    {
        List<MatchThree_Cell> candyLine = new List<MatchThree_Cell>();
        candyLine.Add(targetCell);
        for (int i = 0; i < 2; i++)
        {
            MatchThree_Cell cell = targetCell;
            if (i == 1)
            {
                direction = GetOppositeDirection(direction);
            }

            while (true)
            {
                cell = cell.GetNeighbour(direction);
                if (cell == null)
                {
                    break;
                }
                if (cell.Candy.CandyData.Id == targetCell.Candy.CandyData.Id)
                {
                    candyLine.Add(cell);
                }
                else
                {
                    break;
                }
            }
        }

        return candyLine;
    }

    private static Direction GetLineDirection(MatchThree_Cell targetCell)
    {
        Direction direction = Direction.None;
        for (int i = 0; i < 4; i++)
        {
            int match = 1;
            direction = (Direction) i;
            MatchThree_Cell cell = targetCell.GetNeighbour(GetOppositeDirection(direction));
            if (cell != null && cell.Candy.CandyData.Id == targetCell.Candy.CandyData.Id)
            {
                match++;
            }
            
            cell = targetCell.GetNeighbour(direction);
            if (cell == null)
            {
                //Debug.Log($"No cell: {direction}");
                continue;
            }
            
            if (cell.Candy.CandyData.Id != targetCell.Candy.CandyData.Id)
            {
                continue;
                //Debug.Log($"True: {direction} - {cell.Candy.CandyData.Name}");
            }
            //Debug.Log($"False: {direction} - {cell.Candy.CandyData.Name}");
            match++;
            if (match == 3)
            {
                //Debug.Log($"True: {direction} - {match}");
                break;
            }
            cell = cell.GetNeighbour(direction);
            if (cell == null)
            {
                //Debug.Log($"No cell: {direction}");
                continue;
            }
                
            if (cell.Candy.CandyData.Id != targetCell.Candy.CandyData.Id)
            {
                continue;
                //Debug.Log($"True: {direction} - {cell.Candy.CandyData.Name}");
            }

            match++;
            Debug.Log($"True: {direction} - {match}");
            break;
        }

        return direction;
    }

    private void Awake()
    {
       // m_ResetButton.onClick.AddListener(Restart);
    }

    private void Start()
    {
        
        MainCamera = Camera.main;
        
        m_Field.Init();

        var firstCell = MatchThree_Field.GetCell(0, 0);

        MatchThree_Cell cell = firstCell;
        while (cell)
        {
            SetupCandiesLine(cell, Direction.Right);
            cell = cell.GetNeighbour(Direction.Up);
        }
    }

    private void Update()
    {
        m_Score.text = Score.ToString();
    }

    private void SetupCandiesLine(MatchThree_Cell firstCell, Direction direction)
    {
        MatchThree_Cell cell = firstCell;
        while (cell)
        {
            MatchThree_Candy newCandy = m_Types.GetRandomCandy();
            //пробуем генерить пока не получим разрешенную кoнефетку
            //типов конфеток должно быть 5 или более - иначе возможен вариант вечного цикла
            while (!IsFreeCandyPlacement(cell, newCandy.CandyData.Id))
            {
                Destroy(newCandy.gameObject);
                newCandy = m_Types.GetRandomCandy();
            }

            cell.Candy = newCandy;
            cell.Candy.transform.position = cell.transform.position;
            cell = cell.GetNeighbour(direction);
        }
    }
    
    
}