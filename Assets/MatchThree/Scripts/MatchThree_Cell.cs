﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

//2 and 4 - horiz
//1 and 3 - vert axis
public enum Direction
{
    None = -1,
    Left = 0,
    Right = 2,
    Up = 1,
    Down = 3
}


public class Neighbour
{
    public Direction Direction;
    public MatchThree_Cell Cell;
}

public class MatchThree_Cell : MonoBehaviour
{
    public MatchThree_Candy Candy;
    public int YPos;
    private readonly Neighbour[] neighbours = new Neighbour[4];

    public MatchThree_Cell GetNeighbour(Direction direction) // отдать соседа
    {
    
        return neighbours.FirstOrDefault(n => n != null && n.Direction == direction)?.Cell;
    }

    public void SetNeighbour(Direction direction, MatchThree_Cell cell) // задать соседа
    {
        if (GetNeighbour(direction))
        {
            return;
        }

        neighbours[(int) direction] = new Neighbour()
        {
            Direction = direction,
            Cell = cell
        };
    }
}
