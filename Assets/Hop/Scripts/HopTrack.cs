﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HopTrack : MonoBehaviour
{
    [SerializeField] private GameObject m_Platform;
    [SerializeField] private bool m_UseRandomSeed;
    [SerializeField] private int m_Seed=123456;
    private List<GameObject> platforms = new List<GameObject>();
    
    
    // Start is called before the first frame update
    private void Start()
    {
        platforms.Add(m_Platform);

        if (m_UseRandomSeed)
        {
            Random.InitState(m_Seed);
        }

        float range = 0;
        for (int i = 0; i < 25; i++)
        {
            
            GameObject obj =  Instantiate(m_Platform, transform);
            Vector3 pos = Vector3.zero;
            pos.z = 2 * (i + 1);// + range;
            pos.x = Random.Range(-1, 2);
            obj.transform.position = pos;
            obj.name = $"Platform_{i+1}";
            platforms.Add(obj);
        }
        
    }

    public GameObject FirstPlatform()
    {
        GameObject firstPlatform = platforms[0];
        return firstPlatform;
    }


    public bool IsBallOnPlatform(Vector3 position, ref GameObject nearestPlatform)
    {
        position.y = 0f;
        //nearestPlatform = platforms[0];
        int a = platforms.IndexOf(nearestPlatform);
        if (nearestPlatform.GetComponent<HopPlatform>().Parameters.JumpDistance > 0)
        {
            a += 1;
        }

        if (a + 1 >= platforms.Count)
        {
            return false;
        }
        for (int i = a+1; i < platforms.Count; i++)
        {
            var platformZ = platforms[i].transform.position.z;
            if (platformZ + 0.5f < position.z)
            {
                continue;
            }

            if (platformZ - position.z > 0.5f)
            {
                continue;
            }

            nearestPlatform = platforms[i];
            break;
        }

        
        float minX = nearestPlatform.transform.position.x - 0.5f;
        float maxX = nearestPlatform.transform.position.x + 0.5f;

        if (position.x >= minX && position.x <= maxX)
        {
            var platform = nearestPlatform.GetComponent<HopPlatform>();
            platform.SetGreen();
            nearestPlatform.name  = $"{nearestPlatform.name} passed";
            
            return true;
        }
        else
        {
            var platform = nearestPlatform.GetComponent<HopPlatform>();
            platform.SetRed();
            nearestPlatform.name  = $"{nearestPlatform.name} failed";
            
            
            return false;
        }
        //return position.x >= minX && position.x <= maxX;
    }

    
    
}
