﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HopPlayer : MonoBehaviour
{
    [SerializeField] private AnimationCurve m_JumpCurve;
    [SerializeField] private float m_JumpHeight = 1f;
    [SerializeField] private float m_JumpDistance = 2f;
    [SerializeField] private float m_BallSpeed = 1f;
    [SerializeField] private HopInput m_Input;
    [SerializeField] private HopTrack m_Track;
    

    private float _iteration;//цикл прыжка
    private float _startZ; //точка начала прыжка
    private GameObject currentPlatform;

    private int _score=0;

    // Update is called once per frame
    private void Update()
    {
        if (currentPlatform == null)
        {
            currentPlatform = m_Track.FirstPlatform();
        }

        var platform = currentPlatform.GetComponent<HopPlatform>();
        
        float jumpHeight = m_JumpHeight + platform.Parameters.JumpHeight;
        float jumpDistance = m_JumpDistance + platform.Parameters.JumpDistance;
        float ballSpeed = m_BallSpeed + platform.Parameters.BallSpeed;
        
        
        var pos = transform.position;
        //mooving left/right
        pos.x = Mathf.Lerp(pos.x, m_Input.Strafe, Time.deltaTime * 5f);
        //jump
        pos.y = m_JumpCurve.Evaluate(_iteration) * jumpHeight;
        //mooving forward
        pos.z = _startZ + _iteration * jumpDistance;
        transform.position = pos;

        _iteration += Time.deltaTime * ballSpeed;

        if (_iteration < 1f)
        {
            return;
        }

        _iteration = 0;
        _startZ += m_JumpDistance + platform.Parameters.JumpDistance;

        if (m_Track.IsBallOnPlatform(transform.position, ref currentPlatform))
        {
            _score++;
            return;
        }

        
        Debug.Log($"Your score is {_score} platforms");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }
}
