﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlatformParameters
{
    public float JumpHeight { get; set; }
    public float JumpDistance { get; set; }
    public float BallSpeed { get; set; }
    
    /// <summary>
    /// 0 - basic
    /// 1 - fast jump
    /// 2 - high jump
    /// 3 - long jump
    /// </summary>
    public int Type { get; set; }
    public string Name { get; set; }
    
}


public class HopPlatform : MonoBehaviour
{
    [SerializeField] private GameObject m_baseView;

    [SerializeField] private GameObject m_GreenView;
    [SerializeField] private GameObject m_RedView;
    
    [SerializeField] private float m_LongJumpInc;
    [SerializeField] private float m_HighJumpInc;
    [SerializeField] private float m_FastJumpInc;

    
    public PlatformParameters Parameters;
    
    
    public void SetGreen()
    {
        m_baseView.SetActive(false);
        m_GreenView.SetActive(true);
        Invoke(nameof(SetBase),0.5f);
    }
    
    public void SetRed()
    {
        m_baseView.SetActive(false);
        m_GreenView.SetActive(false);
        m_RedView.SetActive(true);
        //Invoke(nameof(SetBase),0.5f);
    }
    
    private void SetBase()
    {
        m_baseView.SetActive(true);
        m_GreenView.SetActive(false);
        m_RedView.SetActive(false);
        
    }
    

    private void Start()
    {
        int type = Random.Range(0, 9);
        var cubeRenderer = m_baseView.GetComponent<Renderer>();
        switch (type)
        {
            case 1:
                Parameters = new PlatformParameters()
                {
                    JumpDistance = 0,
                    JumpHeight = 0,
                    BallSpeed = m_FastJumpInc,
                    Type = type,
                    Name = "fast",
                };
                cubeRenderer.material.SetColor("_Color", Color.blue);
                break;
            case 2:
                Parameters = new PlatformParameters()
                {
                    JumpDistance = 0,
                    JumpHeight = m_HighJumpInc,
                    BallSpeed = 0,
                    Type = type,
                    Name = "high",
                };
                cubeRenderer.material.SetColor("_Color", Color.cyan);
                break;
            case 3:
                Parameters = new PlatformParameters()
                {
                    JumpDistance = m_LongJumpInc,
                    JumpHeight = 0,
                    BallSpeed = 0,
                    Type = type,
                    Name = "long",
                };
                cubeRenderer.material.SetColor("_Color", Color.yellow);
                break;
            default:
                Parameters = new PlatformParameters()
                {
                    JumpDistance = 0,
                    JumpHeight = 0,
                    BallSpeed = 0,
                    Type = 0,
                    Name = "basic",
                };
                break;
        }
        SetBase();
    }
    
}


