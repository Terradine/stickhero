﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBoxAdaptrer : MonoBehaviour,IHitBox
{
    [SerializeField] private GameObject _hitTarget;
    private IHitBox _hitBox;

    private void Start()
    {
        _hitBox = _hitTarget.GetComponent<IHitBox>();
    }

    private void Reset()
    {
        var hit = GetComponentInParent<IHitBox>();
        if (hit != null && hit!=this)
        {
            _hitTarget = (hit as MonoBehaviour)?.gameObject;
        }
    }

    public int Health => _hitBox.Health;
    public void Hit(int damage)
    {
        _hitBox.Hit(damage);
    }

    public void Die()
    {
        _hitBox.Die();
    }
}
