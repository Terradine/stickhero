﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private float leftBound;
    [SerializeField] private float rightBound;
    // Start is called before the first frame update
   

    // Update is called once per frame
    private void Update()
    {
        var pos = target.position;
        pos.y = transform.position.y;
        pos.z = transform.position.z;
        pos.x = Mathf.Lerp(transform.position.x, target.position.x, Time.deltaTime * 5f);
        pos.x = Mathf.Clamp(pos.x, leftBound, rightBound);
        transform.position = pos;

    }
}
