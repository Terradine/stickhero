﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public static float _horizontalAxis;
    public static event Action<float> JumpAction;
    public static event Action<string> FireAction;
    private float _jumpTimer;
    private Coroutine _waitForJumpCoroutine;
    
    // Start is called before the first frame update
    void Start()
    {
        _horizontalAxis = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        _horizontalAxis = Input.GetAxis("Horizontal");
        if (Input.GetButtonDown("Jump"))
        {
            if (_waitForJumpCoroutine == null)
            {
                _waitForJumpCoroutine = StartCoroutine(WaitJump());
                return;
            }

            _jumpTimer = Time.time;

        }

        if (Input.GetButtonDown("Fire1"))
        {
            FireAction?.Invoke("Fire1");
        }
        
        if (Input.GetButtonDown("Fire2"))
        {
            FireAction?.Invoke("Fire2");
        }
    }

    private void OnDestroy()
    {
        _horizontalAxis = 0f;
    }

    private IEnumerator WaitJump()
    {
        yield return new WaitForSeconds(0.2f);
        if (JumpAction != null)
        {
            //TODO make it constants
            float force = Time.time - _jumpTimer < 0.2f ? 1.25f : 1f;
            JumpAction.Invoke(force);
        }

        _waitForJumpCoroutine = null;
    }
}
