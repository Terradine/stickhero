﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour,IPlayer,IHitBox
{
    [SerializeField] private int health = 1;
    private PlayerWeapon[] _weapons;
   
    public void RegisterPlayer()
    {
        GameManager manager = FindObjectOfType<GameManager>();
        if (manager.Player == null)
        {
            manager.Player = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Awake()
    {
        RegisterPlayer();
        _weapons = GetComponents<PlayerWeapon>();
        InputController.FireAction += Attack;
    }

    private void OnDestroy()
    {
        InputController.FireAction -= Attack;
    }


    public int Health
    {
        get => health;
        private set
        {
            health = value;
            if (health <= 0)
            {
                Die();
            }
        }
    }
    public void Hit(int damage)
    {
        health -= damage;
    }

    public void Die()
    {
        print("Player died");
    }

    private void Attack(string button)
    {
        foreach (var weapon in _weapons)
        {
            if (weapon.ButtonName == button)
            {
                weapon.SetDamage();
            }


        }
    }
}
