﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [SerializeField] private float _movementDistance = 3f;

    private Coroutine moveUpCoroutine;
    
    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (moveUpCoroutine==null && other.GetComponent<Player>())
        {
            moveUpCoroutine = StartCoroutine(MoveUp());
                    
        }
        
    }

    private IEnumerator MoveUp()
    {
        _animator.SetTrigger("Start");
        
        float distance = 0f;
        while (distance < _movementDistance)
        {
            var shift = _movementDistance * Time.deltaTime;
            transform.Translate(Vector3.up * shift);
            distance += shift;
            
            yield return null;
        }
        
        Destroy(gameObject);
        
    }
   
}
