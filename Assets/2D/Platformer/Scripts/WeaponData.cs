﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "data",menuName = "Objects/WeaponObject", order = 1)]
public class WeaponData : ScriptableObject
{
    public string WeaponName = "Weapon";
    public int WeaponDamage = 1;
    public float WeaponRange = 1f;
    public float FireRate = 1f;
    public GameObject Projectile;
    public float ProjectileSpeed = 1f;

}
