﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticObject : MonoBehaviour,IHitBox
{
    
    [SerializeField] private LevelObjectData _objectData;
    
    private Rigidbody2D _rig;
    private int health = 1;
    
    
    
    public int Health
    {
        get => health;
        private set
        {
            health = value;
            if (health <= 0)
            {
                Die();
            }
        }
    }
    public void Hit(int damage)
    {
        health -= damage;
    }

    public void Die()
    {
        print("Static Object destroyed");
    }

    private void Start()
    {
        _rig = GetComponent<Rigidbody2D>();
        Health = _objectData.Health;
        if (_rig != null)
        {
            _rig.bodyType = _objectData.Static ? RigidbodyType2D.Static : RigidbodyType2D.Dynamic;
        }
    }
    
    
    [ContextMenu("Rename")]
    private void Rename()
    {
        if (_objectData != null)
        {
            gameObject.name = _objectData.Name;
        }
    }
    
    [ContextMenu("CopyUp")]
    private void CopyUp()
    {
        var copy = Instantiate(gameObject);
        var pos = copy.transform.position;
        pos.y += 2;
        copy.transform.position = pos;
    }
}
