﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRangeWeapon : MonoBehaviour,IDamager
{

    [SerializeField] private WeaponData _weaponData;
    [SerializeField] private Transform _attackPoint;
    [SerializeField] private string buttonName = "Fire2";
    [SerializeField] private Animator _animator;


    private float _lastAttackTime;

    public int Damage => _weaponData.WeaponDamage;
    public string ButtonName => buttonName;

    public void SetDamage()
    {
        if (Time.time - _lastAttackTime < _weaponData.FireRate)
        {
            return;
        }

        _lastAttackTime = Time.time;
        _animator.SetTrigger("Attack");
        var target = GetTarget();
        target?.Hit(Damage);
    }

    private IHitBox GetTarget()
    {
        IHitBox target = null;
        RaycastHit2D hit = Physics2D.Raycast(_attackPoint.position, _attackPoint.right, _weaponData.WeaponRange);
        if (hit.collider != null)
        {
            target = hit.transform.gameObject.GetComponent<IHitBox>();


        }

        return target;

    }
}
