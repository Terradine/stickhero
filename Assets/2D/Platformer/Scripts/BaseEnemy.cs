﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum EEnemyState
{
    Sleep,
    Wait,
    StartWalking,
    Walk,
    StartAttack,
    Attack,
    CowardlyRun,
}
public class BaseEnemy : MonoBehaviour,IEnemy,IHitBox
{
    [SerializeField] private int health = 2;
    //State Machine
    [SerializeField] private Animator animator;
    [SerializeField] private Transform checkGroundPoint;
    [SerializeField] private Transform checkAttackPoint;
    [SerializeField] private Transform graphics;
    [SerializeField] private bool IsCoward;
    [SerializeField] private Transform player;

    private GameManager gameManager;
    private EEnemyState currentState = EEnemyState.Sleep;
    private EEnemyState nextState;
    
    private float wakeUpTimer;
    private float waitTimer;
    private float attackTimer;
    private float currentDirection = 1f;
    
    
    
    public void RegisterEnemy()
    {
        gameManager = FindObjectOfType<GameManager>();
        gameManager.Enemies.Add(this);
    }

    private void Awake()
    {
        RegisterEnemy();
        wakeUpTimer = Time.time + 1f;
        
    }

    private void Update()
    {
        switch (currentState)
        {
                case  EEnemyState.Sleep:
                    Sleep();
                    break;
                case EEnemyState.Wait:
                    Wait();
                    break;
                case EEnemyState.StartWalking:
                    animator.SetInteger("Walking",1);
                    currentState = EEnemyState.Walk;
                    break;
                case EEnemyState.Walk:
                    Walk();
                    break;
                case EEnemyState.StartAttack:
                    animator.SetTrigger("Attack");
                    ((IHitBox) gameManager.Player).Hit(1);
                    currentState = EEnemyState.Attack;
                    break;
                case EEnemyState.Attack:
                    Attack();
                    break;
                case EEnemyState.CowardlyRun:
                    CowardlyRun();
                    break;
                
                
                default:
                    break;
                    
                    
        }
    }
    
    public int Health
    {
        get => health;
        private set
        {
            health = value;
            if (health <= 0)
            {
                Die();
            }
        }
    }
    
    public void Hit(int damage)
    {
        
        health -= damage;
        Health = health;
        if (IsCoward == true)
        {
            currentState = EEnemyState.CowardlyRun;
        }
       
    }

    public void Die()
    {
       animator.SetTrigger("Die");
        Destroy(this);
        Destroy(gameObject, 1f);
    }

    private void StartSleeping(float sleepTime = 1f)
    {
        wakeUpTimer = Time.time + sleepTime;
        currentState = EEnemyState.Sleep;
    }
    private void Sleep()
    {
        if (Time.time >= wakeUpTimer)
        {
            WakeUp();
        }
        
    }

    private void WakeUp()
    {
        var playerPosition = ((MonoBehaviour) gameManager.Player).transform.position;
        if (Vector3.Distance(transform.position, playerPosition) > 20f)
        {
            StartSleeping();
            return;
        }

        currentState = EEnemyState.Wait;
        nextState = EEnemyState.StartWalking;
        waitTimer = Time.time + 0.1f;
    }
    private void Wait()
    {
        if (Time.time >= waitTimer)
        {
            currentState = nextState;
        }
    }

    private void Walk()
    {
        Walk(1);
    }
    private void Walk(int speed)
    {
        transform.Translate(transform.right * Time.deltaTime * currentDirection * speed);
        //Checking movement availability - platform limit
        RaycastHit2D hit = Physics2D.Raycast(checkGroundPoint.position, Vector2.down, 0.3f);
        if (hit.collider == null)
        {
            EnemyTurnAround();
            
            currentState = EEnemyState.Wait;
            waitTimer = Time.time +0.3f;
            
            animator.SetInteger("Walking",0);
            return;
            
        }

        hit = Physics2D.Raycast(checkAttackPoint.position, checkAttackPoint.right, 0.3f);
        if (hit.collider == null)
        {
            return;
        }

        var player = hit.collider.GetComponent<Player>();
        if (player)
        {
            currentState = EEnemyState.StartAttack;
        }

    }
    private void Attack()
    {
        if (Time.time < attackTimer)
        {
            return;
        }

        currentState = EEnemyState.Wait;
        nextState = EEnemyState.StartWalking;
        waitTimer = Time.time + 0.2f;
    }

    private void CowardlyRun()
    {
        int speed = 2;

        if (player.position.x < gameObject.transform.position.x && currentDirection < 0 ||
            player.position.x > gameObject.transform.position.x && currentDirection > 0)
        {
            EnemyTurnAround();
        }
        
        
        Walk(speed);
    }

    private void EnemyTurnAround()
    {
        currentDirection *= -1;
        float angle = currentDirection > 0 ? 0f:180f;
        graphics.localEulerAngles = new Vector3(0,angle,0f);
    }
}
