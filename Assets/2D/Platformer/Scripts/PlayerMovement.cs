﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]

public class PlayerMovement : CharacterMovement
{
    [SerializeField] private float _maxSpeed = 8f;
    [SerializeField] private Transform _graphics;
    [SerializeField] private float _jumpForce = 5f;
    [SerializeField] private Animator _animator;
    private Rigidbody2D _rig;
    private bool _isRunning = false;

    private void Start()
    {
        _rig = GetComponent<Rigidbody2D>();
        InputController.JumpAction += OnJumpAction;
    }

    private void OnDestroy()
    {
        InputController.JumpAction -= OnJumpAction;
    }

    private void FixedUpdate()
    {
        if (IsFreezing)
        {
            Vector2 velocity = _rig.velocity;
            velocity.x = 0f;
            _rig.velocity = velocity;
            return;
        }
        Vector2 direction = new Vector2(InputController._horizontalAxis, 0f);
        if (!IsGrounded())
        {
            direction *= 0.5f;
        }
        Move(direction);
    }

    private void Update()
    {
        if(Input.GetKeyUp(KeyCode.LeftShift))
        {
            _isRunning = !_isRunning;
            Debug.Log($"Running: {_isRunning}");
        }

        if (IsGrounded())
        {
            _animator.SetFloat("Speed",Mathf.Abs(_rig.velocity.x));
        }
        else
        {
            _animator.SetFloat("Speed",0);
        }
        
        if (Mathf.Abs(_rig.velocity.x) < 0.01f)
        {
            return;
        }

        float angle = _rig.velocity.x > 0 ? 0f : 180f;
        _graphics.localEulerAngles = new Vector3(0f, angle, 0f);
    }

    public override void Move(Vector2 direction)
    {
        float speed = _maxSpeed;
        if(_isRunning==true)
        {
            speed *= 1.5f;
        }
        Vector2 velocity = _rig.velocity;
        velocity.x = direction.x * speed;
        _rig.velocity = velocity;
    }

    public override void Stop(float timer)
    {

    }

    public override void Jump(float force)
    {

        _rig.AddForce(new Vector2(0,force),ForceMode2D.Impulse);
    }

    private IEnumerator JumpAnimation(float force)
    {
        _animator.SetTrigger("Jump");
        yield return new WaitForSeconds(0.1f);
        Jump(_jumpForce * force);

    }

    private void OnJumpAction(float force)
    {
        if (IsGrounded() && !IsFreezing)
        {           
            StartCoroutine(JumpAnimation(force));
            
        }
    }

    private bool IsGrounded()
    {
        Vector2 point = transform.position;
        point.y -= 0.1f; //to avoid collision with itself
        RaycastHit2D hit = Physics2D.Raycast(point, Vector2.down, 0.2f);
        return hit.collider != null;
    }


}
