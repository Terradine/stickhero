﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FitInTheHole_Wall

{
    private List<Transform> _cubes;
    private Transform _parent;
    public Transform Parent => _parent;

    public FitInTheHole_Wall(int sizeX, int sizeY, GameObject prefab)
    {
        GenerateWall(sizeX,sizeY,prefab);
    }

    private void GenerateWall(int sizeX, int sizeY, GameObject prefab)
    {
        _cubes= new List<Transform>();
        _parent = new GameObject().transform;
        for (int x = -sizeX + 1; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                var obj = Object.Instantiate(prefab, new Vector3(x, y, 0),Quaternion.identity);
                obj.transform.parent = _parent;
                _cubes.Add(obj.transform);
            }
        }
        
        _parent.position = new Vector3(0f, 0.5f, 0f);
    }

    public void SetupWall(FitInTheHole_Template template, float position)
    {
        _parent.transform.position = new Vector3(0f,0f,position);
        foreach (var cube in _cubes)
        {
            cube.gameObject.SetActive(true);
        }

        if (template == null)
        {
            return;
        }
        var figure = template.GetFigure();
        for(int f=0;f<figure.Length;f++)
        {
            for(int c=0;c<_cubes.Count;c++)
            {
                if (!figure[f] || !_cubes[c])
                {
                    continue;
                }
                if (Mathf.Abs(figure[f].position.x - _cubes[c].position.x) > 0.1f)
                {
                    continue;
                }
                if (Mathf.Abs(figure[f].position.y - _cubes[c].position.y) > 0.1f)
                {
                    continue;
                }
                _cubes[c].gameObject.SetActive(false);
                
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
