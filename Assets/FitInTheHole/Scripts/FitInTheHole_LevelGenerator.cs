﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FitInTheHole_LevelGenerator : MonoBehaviour
{
    [SerializeField] private GameObject m_CubePrefab;
    [SerializeField] private float m_BaseSpeed=2f;
    [SerializeField] private float m_WallDistance = 35f;

    [SerializeField] private FitInTheHole_Template[] m_TemplatePrefabs;
    [SerializeField] private Transform m_FigurePoint;

    //templates samples to avoid instantion many times
    private FitInTheHole_Template[] _templates;
    private FitInTheHole_Template _figure; //current figure

    private float _speed;

    private FitInTheHole_Wall _wall;
    // Start is called before the first frame update
    void Start()
    {
        _templates = new FitInTheHole_Template[m_TemplatePrefabs.Length];
        for (int i = 0; i < _templates.Length; i++)
        {
            _templates[i] = Instantiate(m_TemplatePrefabs[i]);
            _templates[i].gameObject.SetActive(false);
            _templates[i].transform.position = m_FigurePoint.position;
        }
        _wall = new FitInTheHole_Wall(5,5,m_CubePrefab);
        SetupTemplate();
        _wall.SetupWall(_figure,m_WallDistance);
        _speed = m_BaseSpeed;

    }

    // Update is called once per frame
    private void Update()
    {
        _wall.Parent.Translate(Time.deltaTime * _speed * Vector3.back);
        if (_wall.Parent.transform.position.z > m_WallDistance * -1f)
        {
            return;
        }
        SetupTemplate();
        _wall.SetupWall(_figure, m_WallDistance);
        
        

    }

    private void SetupTemplate()
    {
        if (_figure)
        {
            _figure.gameObject.SetActive(false);
        }

        var rand = Random.Range(0, _templates.Length);
        _figure = _templates[rand];
        _figure.gameObject.SetActive(true);
        _figure.SetupRandomFigure();
    }
}
